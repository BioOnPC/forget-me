local ForgetMePlus = RegisterMod("Forget Me+", 1)
local alphaMod

local collectible_discord

--- PASSIVE ITEM FUNCTIONALITY ---
local function cacheDiscord(player, cache_flag)
	if cache_flag == CacheFlag.CACHE_LUCK then
		player.Luck = player.Luck + 1
	end
end

debug_text = ""

--- ENTITY SHIT ---
function ForgetMePlus.tearAppear(entity)
	entity = entity:ToTear()
	entity_data = entity:GetData()
	local player = AlphaAPI.GAME_STATE.PLAYERS[1]
	if entity.SpawnerType == EntityType.ENTITY_PLAYER then
		if player:HasCollectible(collectible_discord.id) and entity_data.discorded == nil and AlphaAPI.getLuckRNG(10,20) then -- the luck rng is based on 1000 instead of 100?? remember that
			entity_data.discorded = 1
			local DISCORD_TEAR = player:FireTear(entity.Position, entity.Velocity)
			DISCORD_TEAR:ChangeVariant(TearVariant.BLOOD)
			DISCORD_TEAR_DATA = DISCORD_TEAR:GetData()
			DISCORD_TEAR_DATA.discorded = 1
			DISCORD_TEAR.TearFlags = entity.TearFlags
			DISCORD_TEAR.Scale = entity.Scale
			DISCORD_TEAR.Height = entity.Height
			
			if player:GetFireDirection() == 1 or player:GetFireDirection() == 3 then
				DISCORD_TEAR.Position.X = entity.Position.X + 8
				entity.Position.X = entity.Position.X - 8
			else
				DISCORD_TEAR.Position.Y = entity.Position.Y + 8
				entity.Position.Y = entity.Position.Y - 8
			end
			
		end
	end
end

local function start()
	alphaMod = AlphaAPI.registerMod(ForgetMePlus)

	--- PASSIVE ITEMS INIT ---
	
	-- DOUBLES TEARS RANDOMLY BASED ON LUCK + 1 LUCK --
	collectible_discord = alphaMod:registerItem("Discord", nil, {CacheFlag.CACHE_LUCK}) --Isaac.GetItemIdByName("Discord")
	collectible_discord:addCallback(AlphaAPI.Callbacks.ITEM_CACHE, cacheDiscord)
	alphaMod:addCallback(AlphaAPI.Callbacks.ENTITY_APPEAR, ForgetMePlus.tearAppear, EntityType.ENTITY_TEAR)
end

function ForgetMePlus:debug_text()
	Isaac.RenderText(debug_text, 50, 50, 0, 255, 0, 255)
end

ForgetMePlus:AddCallback(ModCallbacks.MC_POST_RENDER, ForgetMePlus.debug_text)

-- 'start' is the name of the function
--  that fires when AlphaAPI is loaded
local START_FUNC = start
--------------------------------------
if AlphaAPI then START_FUNC()
else if not __alphaInit then
__alphaInit={} end __alphaInit
[#__alphaInit+1]=START_FUNC end
--------------------------------------